# NTrip client (with GPS Net)
A ROS wrappers for interfacing with a NTrp server using GPS Net

## ROS1
1. Clone repo
    ```
    cd <your-ros1-ws>/src
    git clone -b ros1 git@gitlab.com:uaswork/rtk-ntrip.git
    ```


## ROS2
1. Clone repo
    ```
    cd <your-ros2-ws>/src
    git clone -b ros2 git@gitlab.com:uaswork/rtk-ntrip.git
    ```
